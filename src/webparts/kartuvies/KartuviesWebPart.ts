import * as React from 'react';
import * as ReactDom from 'react-dom';
import { Version } from '@microsoft/sp-core-library';
import {  BaseClientSideWebPart,
          PropertyPaneSlider} from '@microsoft/sp-webpart-base';
import {
  IPropertyPaneConfiguration
} from '@microsoft/sp-property-pane';

import * as strings from 'KartuviesWebPartStrings';
import Kartuvies from './components/Kartuvies';
import { IKartuviesProps } from './components/IKartuviesProps';
import { ThemeProvider, ThemeChangedEventArgs, IReadonlyTheme } from '@microsoft/sp-component-base';
export interface IKartuviesWebPartProps {
  description: string;
  timeLeft: string;
  showTime: boolean;
}

import { CalloutTriggers } from '@pnp/spfx-property-controls/lib/Callout';
import { PropertyFieldCheckboxWithCallout } from '@pnp/spfx-property-controls/lib/PropertyFieldCheckboxWithCallout';
import { string } from 'prop-types';


export default class KartuviesWebPart extends BaseClientSideWebPart<IKartuviesWebPartProps> {



  _themeVariant: IReadonlyTheme | undefined;


  public render(): void {

    
    
      
    const element: React.ReactElement<IKartuviesProps > = React.createElement(
      Kartuvies,
      {
        description: this.properties.description,
        siteurl: this.context.pageContext.web.absoluteUrl,
        client: this.context.spHttpClient,
        timeLeft: this.properties.timeLeft,
        showTime: this.properties.showTime,
        themeVariant: this._themeVariant
      }
    );

    ReactDom.render(element, this.domElement);
  }

  protected onInit(): Promise<void> {
    console.log(this.context);
    const _themeProvider = this.context.serviceScope.consume(ThemeProvider.serviceKey);
    this._themeVariant = _themeProvider.tryGetTheme();
    if(this._themeVariant == null){
      console.log("NERA TEMOS")
    }
    _themeProvider.themeChangedEvent.add(this, this._handleThemeChangedEvent);
    console.log(_themeProvider.tryGetTheme());
    return super.onInit();
}

private _handleThemeChangedEvent(args: ThemeChangedEventArgs): void {
  this._themeVariant = args.theme;
  this.render();
}

  protected onDispose(): void {
    ReactDom.unmountComponentAtNode(this.domElement);
  }

  protected get dataVersion(): Version {
    return Version.parse('1.0');
  }

  protected getPropertyPaneConfiguration(): IPropertyPaneConfiguration {
    return {
      pages: [
        {
          header: {
            description: strings.PropertyPaneDescription
          },
          groups: [
            {
              groupName: strings.BasicGroupName,
              groupFields: [
                PropertyFieldCheckboxWithCallout('showTime', {
                  calloutTrigger: CalloutTriggers.Click,
                  key: 'showTimeID',
                  calloutContent: React.createElement('p', {}, strings.setTimeCalloutContent),
                  calloutWidth: 200,
                  text: strings.setTimeLable,
                  checked: this.properties.showTime
                }),
                PropertyPaneSlider('timeLeft',{
                  label: strings.timeLeftLable,
                  value: 20,
                  min: 0,
                  max: 120,
                  showValue: true,
                  disabled: !this.properties.showTime
                })
              ]
            }
          ]
        }
      ]
    };
  }
}
