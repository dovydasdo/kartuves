import { SPHttpClient } from "@microsoft/sp-http";
import { IReadonlyTheme } from "@microsoft/sp-component-base";

export interface IKartuviesProps {
  description: string;
  client: SPHttpClient;
  siteurl: string;
  showTime: boolean;
  timeLeft: string;
  themeVariant: IReadonlyTheme | undefined;
  keydown: any;
}

export interface IKartuviesState {
  displayWord: string[];
  triesLeft: number;
  wordSet: string;
  difficulty: string;
  isLoaded: boolean;
  timeCount: number;
}
