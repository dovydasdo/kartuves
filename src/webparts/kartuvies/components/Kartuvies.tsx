import * as React from 'react';
import styles from './Kartuvies.module.scss';
import { IKartuviesProps, IKartuviesState } from './IKartuviesProps';
import Popup from "reactjs-popup";
import axios from 'axios';
import * as strings from "KartuviesWebPartStrings";
import { IReadonlyTheme } from '@microsoft/sp-component-base';
import { registerBeforeUnloadHandler } from '@microsoft/teams-js';
import keydown from 'react-keydown';
var apiPath = "/_api/web/lists/getByTitle('Žodžiai')/items";
var ZODIS = "";
var HINT = "";
var letters = ["A", "Ą", "B", "C", "Č","D", "E", "Ę", "Ė", "F", "G", "H", "I", "Į", "Y", "J", "K", "L", "M", "N", "O", "P", "R", "S", "Š", "T", "U", "Ų", "Ū", "V", "Z", "Ž" ];
var items = [];
var guessedWords = [];

interface Window {
  __themeState__: any;
}

declare var window: Window;
@keydown
export default class Kartuvies extends React.Component<IKartuviesProps, IKartuviesState, any> {

  timerID = 0;

  constructor(props){

    super(props);

    this.state= {
      displayWord: [],
      triesLeft: 6,
      wordSet: "GYVŪNAI",
      difficulty: "LENGVAS",
      isLoaded: false,
      timeCount: parseInt(this.props.timeLeft, 10)
    }

    this.handleWordSetChange = this.handleWordSetChange.bind(this);
    this.handleDifficultyChange = this.handleDifficultyChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.setWord = this.setWord.bind(this);

  }

 componentDidMount(){
   
   console.log(window.__themeState__.theme);
    this.timerID = setInterval(
      () => this.tick(),
      1000
    );
    this.getData();
    var tempWord = [];
    for(var i = 0; i < ZODIS.length; i++){
            tempWord.push("-");
          }

          this.setState({
            displayWord: tempWord,
            isLoaded: true
          })

          console.log(this.props.themeVariant);
  }

  componentWillReceiveProps( { keydown } ) {
    if ( keydown.event ) {
      // inspect the keydown event and decide what to do
      let input = keydown.event.key;
      console.log(keydown.event);
      for(let i = 0; i< letters.length; i++){
        if(letters[i] == input.toUpperCase()){
          this.checkLetters(letters[i])
        }
      }
    }
  }

  tick(){
    let newTime = this.state.timeCount;
    if(this.props.showTime == false){
      return;
    }
      newTime = newTime - 1;
    
      this.setState({
      timeCount: newTime
    })
    if(newTime <= 0)
      this.disableButtons();
    if(this.state.triesLeft <= 0 || this.isAnswered()){
      this.setState({
        timeCount: parseInt(this.props.timeLeft, 10)
      })
    }

  }

  setWord(){
    let index = -1;
    for(let i = 0; i < items.length; i++){
      if(items[i]["Sunkumas"] == this.state.difficulty && items[i]["Kategorija"] == this.state.wordSet && this.usedWord(i) == false){
        index = i;
        break;
      }
    }
    if(index == -1){//Jei baigesi zodziai pradedam is naujo
      guessedWords = [];

      for(let i = 0; i < items.length; i++){
        if(items[i]["Sunkumas"] == this.state.difficulty && items[i]["Kategorija"] == this.state.wordSet && this.usedWord(i) == false){
          index = i;
          break;
        }
      }
    }
    ZODIS = items[index]["Žodis"];
    guessedWords.push(index);
    this.reset();

  }

  usedWord(indx){
    let used = false;
    for(let i = 0; i <= guessedWords.length; i++){
      if(indx == guessedWords[i]){
        used = true;
      }
    }
    return used;
  }

  disableButtons(){
    for(let i = 0; i < letters.length; i++){
      document.getElementById("Button-" + letters[i]).setAttribute('disabled', 'disabled');
    }
  }

  enableButtons(){
    for(let i = 0; i < letters.length; i++){
      document.getElementById("Button-" + letters[i]).removeAttribute('disabled');
    }
  }

  async getData(){
    let result = await axios.get(apiPath);
      if(!(items.length >= result.data.value.length)){//kaskodel 4 karus iraso vertes is listo
        for (var i = 0; i < result.data.value.length; i++) {
          items.push({
            Žodis: String(result.data.value[i]["Title"]),
            Sunkumas: String(result.data.value[i]["Sunkumas"]),
            Kategorija: String(result.data.value[i]["Kategorija"])
          });
          
        }   
        this.setWord();
      }
  }

  isAnswered(){
    let answered = true;
    let word = this.state.displayWord;
    for(var i = 0; i < ZODIS.length; i++){
      if(word[i] == "-"){
        answered = false;
      }
    }
    return answered;
  }

  reset(){
    var tempWord = [];
    for(var i = 0; i < ZODIS.length; i++){
      tempWord.push("-");
    }
    this.enableButtons();
    this.setState({
      displayWord: tempWord,
      triesLeft: 6,
      timeCount: parseInt(this.props.timeLeft, 10)
    })
  }

  checkLetters(letter){
    document.getElementById("Button-" + letter).setAttribute('disabled', 'disabled');
    let isInWord = false;
    let tempWord = this.state.displayWord;
    let tempTires = this.state.triesLeft;
    for(var i = 0; i < ZODIS.length; i++){
      if(ZODIS[i] == letter){
        isInWord = true;
        tempWord[i] = ZODIS[i];
      }
    }

    if(isInWord){
      this.setState({
        displayWord: tempWord,
      })

    }
    else{
      tempTires--;
      if(tempTires <= 0){
        this.disableButtons();
      }
      this.setState({
        triesLeft: tempTires 
      })
    }
  }

  handleWordSetChange(event){
    guessedWords = [];
    switch(event.target.value){
      case "0":
        this.setState({wordSet: "GYVŪNAI"});
        break;
      case "1":
        this.setState({wordSet: "ŠALYS"});
        break;
      case "2":
        this.setState({wordSet: "AUGALAI"});
        break;
      default:
        this.setState({wordSet: "ERROR"});
        break;
    }
    
  }

  handleDifficultyChange(event){
    guessedWords = [];
    switch(event.target.value){
      case "0":
        this.setState({difficulty: "LENGVI"});
        break;
      case "1":
        this.setState({difficulty: "VIDUTINIS"});
        break;
      case "2":
        this.setState({difficulty: "SUNKUS"});
        break;
      default:
        this.setState({difficulty: "ERROR"});
        break;
    }
  }

  handleKeyPress = event =>{
    console.log(event);
    console.log(this.state.timeCount);
    this.checkLetters(event);
  }


  handleSubmit(event){
    event.preventDefault();
    this.setWord();
  }

  currentDifficulty(){
    return this.state.difficulty;
  }

  curretWordSet(){
    return this.state.wordSet;
  }

  public render(): React.ReactElement<IKartuviesProps> {

    /*const { semanticColors }: IReadonlyTheme = this.props.themeVariant;*/
    
    return (
      <div className={ styles.kartuvies }
      onKeyPress={(event)=> this.handleKeyPress(event)}
      >
        <div className={ styles.container }>
          <div className={ styles.row }>
            <div className={ styles.column }>
              <div>
                {this.props.showTime ? this.state.timeCount <= 0 ? strings.timeOut: strings.timeLeft + this.state.timeCount + strings.timeSeconds : ""}
              </div>
              
              <span className={ styles.title }>{strings.gameName}<br/> {strings.wordDiffDesc} {this.currentDifficulty()}, {strings.wordCtgDesc} {this.curretWordSet()}</span>
              <p className={ styles.description }>{this.isAnswered() == true ? strings.winMessage : (this.state.triesLeft <= 0 || this.state.timeCount<= 0 ? strings.looseMessage : this.state.displayWord)}</p>
              <p className={styles.description}> {this.isAnswered() == true || this.state.triesLeft <= 0 || this.state.timeCount<= 0 ? ZODIS : ""} </p> 
             
              
              <ul id="buttons"
                  className={styles.buttons}
              >
              {letters.map(item =>{
                return(
                    <button onClick = { ()=>this.checkLetters(item) }  id = { "Button-" + item}
                    
                    className={styles.button}
                    >
                      {item}
                    </button>
                )})}
              </ul>
              
              <div className={styles.newWord}>
                <button className={styles.button} onClick = {()=>this.setWord()}  type='button'>
                  {strings.newWordBtn}
                </button>
                <Popup trigger={<button className={styles.button}  type='button'> {strings.settingsBtn} </button>} modal>
                {close => (
                  <div className={styles.options}>
                    <a className={styles.close} onClick={close}>
                      {/*&times;*/}
                    </a>
                    <div className={styles.optionHeader}> {strings.settingsDisc} </div>
                    <br/>
                    <div className={styles.optionContents}>
                    <form onSubmit={this.handleSubmit}>
                      <label>
                        {strings.settingsCtg}
                        <select value={this.state.wordSet} onChange={this.handleWordSetChange} className={styles.optionsSelect} name={this.state.wordSet}>
                          <option value={0}> {strings.wordCtgAnimals}</option>
                          <option value={1}> {strings.wordCtgCountrys}</option>
                          <option value={2}> {strings.wordCtgPlants}</option>
                        </select>
                      </label>
                      <br/>
                      <label>
                      {strings.settingsDiff} 
                        <select value={this.state.difficulty} onChange={this.handleDifficultyChange} className={styles.optionsSelect} name={this.state.difficulty}>
                          <option value={0}> {strings.wordDiffEasy}</option>
                          <option value={1}> {strings.wordDiffMedium}</option>
                          <option value={2}> {strings.wordDiffHard}</option>
                        </select>
                      </label>
                      <br/><br/>
                      <input type="submit" value={strings.settingsSave} className={styles.button}/>
                      <br/>
                    </form>
                    </div>
                    
                  </div>
                )}
              </Popup>
              
            </div>
              <div className={styles.column2}>
               <img className={styles.drawing} src={ require('./kartuves_img/tries-' + this.state.triesLeft + '.png') }  />
             </div>
            </div>
            
          </div>

        </div>
      </div>
    );
  }
  private getThemeVariablesItems(): Array<any> {
    const variables: any = window.__themeState__.theme;
    const items = [];
    
    for (let varName in variables) {
      if (!variables.hasOwnProperty(varName))
        continue;

      items.push({
        key: varName,
        name: varName,
        value: variables[varName]
      });
    }

    return items;
  }
}
