define([], function() {
  return {
    "PropertyPaneDescription": "Settings",
    "BasicGroupName": "Time",
    "DescriptionFieldLabel": "Description Field",
    "setTimeCalloutContent": "Restrict time to guess a word",
    "setTimeLable": "Restrct time",
    "TimeLeftLable": "Amount of time (seconds)",
    "gameName": "Kartuvės",
    "wordDiffDesc": "Difficulty of words: ",
    "wordDiffEasy": "EASY",
    "wordDiffMedium": "MEDIUM",
    "wordDiffHard": "HARD",
    "wordCtgDesc": "Cathegory: ",
    "wordCtgAnimals": "ANIMALS",
    "wordCtgCountrys": "COUNTRYS",
    "wordCtgPlants": "PLANTS",
    "winMessage": "YOU WON",
    "looseMessage": "YOU LOST",
    "newWordBtn": "NEW WORD",
    "settingsDesc": "Settings meniu",
    "settingsBtn": "SETTINGS",
    "settingsCtg": "Word cathegory ",
    "settingsDiff": "Word difficulty ",
    "settingsSave": "SAVE",
    "timeOut": "YOU RAN OUT OF TIME...",
    "timeLeft": "TIME LEFT: ",
    "timeSeconds": " seconds"
  }
});