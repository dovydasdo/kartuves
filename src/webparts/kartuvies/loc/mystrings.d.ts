declare interface IKartuviesWebPartStrings {
  PropertyPaneDescription: string;
  BasicGroupName: string;
  DescriptionFieldLabel: string;
  setTimeCalloutContent: string;
  setTimeLable: string;
  timeLeftLable: string;
  gameName: string;
  wordDiffDesc: string;
  wordDiffEasy: string;
  wordDiffMedium: string;
  wordDiffHard: string;
  wordCtgDesc: string;
  wordCtgAnimals: string;
  wordCtgCountrys: string;
  wordCtgPlants: string;
  winMessage: string;
  looseMessage: string;
  newWordBtn: string;
  settingsDisc: string;
  settingsBtn: string;
  settingsCtg: string;
  settingsDiff: string;
  settingsSave: string;
  timeOut: string;
  timeLeft: string;
  timeSeconds: string;
}

declare module 'KartuviesWebPartStrings' {
  const strings: IKartuviesWebPartStrings;
  export = strings;
}
