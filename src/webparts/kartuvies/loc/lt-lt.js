define([], function() {
    return {
      "PropertyPaneDescription": "Nustatymai",
      "BasicGroupName": "Laiko",
      "DescriptionFieldLabel": "Description Field",
      "setTimeCalloutContent": "Riboti vieno žodžio spėjimo laiką",
      "setTimeLable": "Riboti laiką",
      "TimeLeftLable": "Laiko spėjimui(sekundės)",
      "gameName": "Kartuvės",
      "wordDiffDesc": "Žodžių sunkumas: ",
      "wordDiffEasy": "LENGVAS",
      "wordDiffMedium": "VIDUTINIS",
      "wordDiffHard": "SUNKUS",
      "wordCtgDesc": "Kategorija: ",
      "wordCtgAnimals": "GYVŪNAI",
      "wordCtgCountrys": "ŠALYS",
      "wordCtgPlants": "AUGALAI",
      "winMessage": "LAIMĖJOTE",
      "looseMessage": "PRALAIMĖJOTE",
      "newWordBtn": "NAUJAS ŽODIS",
      "settingsDesc": "Nustatymų meniu",
      "settingsBtn": "NUSTATYMAI",
      "settingsCtg": "Žodžių kategorijos ",
      "settingsDiff": "Žodžių sunkumas ",
      "settingsSave": "IŠSAUGOTI",
      "timeOut": "Laikas baigėsi...",
      "timeLeft": "Jums liko ",
      "timeSeconds": " sekundžių"
    }
  });